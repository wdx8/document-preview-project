# 文档预览项目Demo
 在项目开发中，遇到很多次有关文件的需求，如不同文件类型的文件上传、文件下载、文件预览。文件上传在[https://qkongtao.cn/?p=1410](https://qkongtao.cn/?p=1410)中有相关大文件分片上传、断点续传及秒传的介绍；文件下载在[https://qkongtao.cn/?p=560#h2-0](https://qkongtao.cn/?p=560#h2-0)的第14个方法中有下载的工具方法介绍；各种文件的预览在项目中用的也比较频繁，大多数情况下的文件预览都会用第三方的服务或者后端服务进行实现，但是也有些情况适合纯web端实现各种文件的预览，本次就记录一下使用纯web端实现各种文档文件的预览，并且全部封装成单独的组件，开箱即用。

**本次实现的文档预览的类型有：docx, xlsx, pptx, pdf,以及纯文本、代码文件和各种图片、视频格式的在线预览**

# 项目安装

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
